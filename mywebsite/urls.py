
from django.conf.urls import url, include
from django.contrib import admin

from . import views
#from blog import views as blogViews
from about import views as aboutViews

urlpatterns = [
    #path('admin/', admin.site.urls),
    #path('/', views.index),
    url(r'^admin/', admin.site.urls),
    #url(r'^blog/$', blogViews.index),
    url(r'^blog/', include('blog.urls')),
    url(r'^about/$', aboutViews.index),
    url(r'^$', views.index),
]
